'''
@Original Author: Ewald Zietsman
@Source URL: https://gist.github.com/ezietsman/097dd1eee738a3ab097c
@Author: Rudolph Lombaard
@Created: 15-March-2014
'''

import subprocess
import time
import matplotlib.style
matplotlib.style.use('fivethirtyeight')
import matplotlib.pyplot as plt
import re
import datetime
import csv


def write_to_csv(ping_list, csvfilename):
    '''
    'Writes a list to csv file
    '''

    # Original source from
    # http://stackoverflow.com/questions/14693646/writing-to-csv-file-python

    with open(csvfilename, 'wb') as fp:
        a = csv.writer(fp, delimiter=',')
        # add header row to list
        output_list = [['ping time [ms]']]

        # iterate through original ping and add each individual value as list
        for row in ping_list:
            # create temporary list
            tmp_list = []
            # append ping value to the temporary list
            tmp_list.append(str(row))
            # append list to the output CSV list
            output_list.append(tmp_list)

        # writes a list of lists to CSV file
        a.writerows(output_list)

        # data = [['Me', 'You'],
        #         ['293', '219'],
        #         ['54', '13']]
        # a.writerows(data)


def make_plots():
    '''
    Makes a plot object and returns a line and axes object
    '''
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    line, = ax1.plot([], [], linewidth=2)

    plt.ylabel('Ping (ms)')
    plt.xlabel('Time (s)')
    plt.subplots_adjust(left=0.1, bottom=0.1)

    return line, ax1


if __name__ == "__main__":

    print("ping.py script Started")

    # cmd = ['ping', '-i', '0.2', '197.84.96.137']
    cmd = ['ping', 'www.google.co.za', '-t']

    starttime = time.time()

    print("subprocess opened")

    prc = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    pings = []
    times = []
    plt.ion()
    line, ax = make_plots()
    plt.show()
    updates = 0

    print("Waiting 120 seconds before closing process and saving results")

    # Compile regex expression
    regex = re.compile("time=(\d+)")

    # while time.time() - starttime < 120:
    while time.time() - starttime < (10*60):
        output = prc.stdout.readline()

        r = regex.findall(output)

        for i in r:
            pings.append(int(i))

        # if no output from process then skip this loop
        if not output:
            continue

        # Only output to terminal if the process have valid output to display
        print("Process output:[%s]" % output)

        if not updates % 5:
            times = [t for t in xrange(len(pings))]
            line.set_ydata(pings)
            line.set_xdata(times)

            # calculate data limits
            ax.relim()
            # autoscale view of chart
            ax.autoscale_view()
            # plot new data
            plt.draw()

        updates += 1

    datetimestr = str(datetime.datetime.now().strftime("%Y%m%d_%H%M"))
    outputfilenamePNG = "../results/%s__pings_timeseries.png" % (datetimestr)
    outputfilenameCSV = "../results/%s__pings_timeseries.csv" % (datetimestr)

    print("Writing image to PNG file [%s]" % (outputfilenamePNG))

    # writes chart to PNG file
    plt.savefig(outputfilenamePNG)

    # writes ping list to CSV
    write_to_csv(pings, outputfilenameCSV)

    print("Ending script ping.py")
